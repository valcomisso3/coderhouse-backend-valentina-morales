
# websockets + handlebars

Servidor basado en Node.JS y express con enpoints y servicios necesarios para gestionar los productos y carritos de compra en el e-commerce, **integrado con vistas y sockets**.


**Información**: Corre en el puerto 8080 y cuenta con 3 grupos de rutas /products y /carts

`http://localhost:8080`

routes

`http://localhost:8080/api/products` -> productos
`http://localhost:8080/api/carts` -> carritos
`http://localhost:8080/` -> views

### Documentación
La documentacion para controlar y ejecutar cada uno de los enpoints esta en el siguente link:
[Documentación de ejecución en postman](https://documenter.getpostman.com/view/11242436/2sA3BrWpD8): :rocket: :sunglasses:    (contiene la colección de todos los enpoints)

El servidor contiene un archivo 'address.js' que contiene las direcciones donde se crearan los productos, los carritos, y el puerto. 

Vistas: 

`http://localhost:8080/` : Muestra la lista de Productos 

`http://localhost:8080/realTimeProducts` : Muestra la lista de Productos en **tiempo real**.

`http://localhost:8080/chat` : Muestra el chat 
#
 Quedo a disposición de cualquier consulta y atenta a cualquier corrección que requiera mi proyecto, deseo que sea una experiencia amena revisarlo.  ¡muchas gracias!  :blush: 


