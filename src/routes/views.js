

var express = require('express');
const io = require('socket.io')

var api = express.Router();

// Mostrar lista de productos 
api.get('/', async (req, res) => {
    const productsJson = req.productsJson
    res.render('home', { productsJson })
})

// Mostrar lista de productos  EN TIEMPO REAL
api.get('/realTimeProducts', async (req, res) => {
    const productsJson = req.productsJson
    res.render('realTimeProducts', { productsJson })
})

api.get('/chat', async (req, res) => {
    const messagesJson = req.messagesJson
    res.render('chat', { messagesJson })
})


module.exports = api; // exporto la configuración

