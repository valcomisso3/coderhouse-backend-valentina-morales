

var express = require('express');

// clases
// - MongoDB
const CartsManagerDB = require('../dao/mongoDB/cartsManagerDB.js')
const ProductManagerDB = require('../dao/mongoDB/productManagerDB.js')

const cartsManagerDB = new CartsManagerDB()  // Genero una instancia de CartsManagerDB
const productsManagerDB = new ProductManagerDB()  // Genero una instancia de ProductManagerDB

var api = express.Router();

// Obtener todos los carritos
api.get('/', async (req, res) => {
    const carts = await cartsManagerDB.getCarts()
    res.send(carts)
})

// Obtener productos de un carrito especifico 
api.get('/:cid', async (req, res) => {
    let cid = req.params.cid // ID del producto que se requiere
    if (cid) {
        const card = await cartsManagerDB.getCartsById(cid)
        res.send(card)
    } else {
        res.send('Error: Se requiere el id del producto')
    }
});

// Añadir nuevo carrito
api.post('/', async (req, res) => {
    var data = req.body
    const result = await cartsManagerDB.addCart(data)
    res.json({ message: result })
})

// Añadir un producto a un carrito especifico
api.post('/:cid/product/:pid', async (req, res) => {
    let pid = req.params.pid // id del producto 
    let cid = req.params.cid // id del carrito 
    const quantity = 1 // cantidad de productos

    if (cid && pid) {
        const productValue = await productsManagerDB.getProductById(pid)
        if (productValue.success == false) {
            // ese producto no existe, no es posible agregarlo al carrito
            res.send(`Error: el producto con id ${pid} no existe, no se puede agregar al carrito `)
        } else {
            const result = await cartsManagerDB.addProductCart(cid, pid, quantity)

            res.json(result)
        }

    } else {
        res.send('Error: Se requiere id del carrito e id del carrito')
    }
})


module.exports = api; // exporto la configuración

