
var express = require('express');

// clases
// - MongoDB
const ProductManagerMD = require('../dao/mongoDB/productManagerDB') // Importo la clase

const productManagerMD = new ProductManagerMD() // Genero una instancia de ProductManagerMD

var api = express.Router();

// Optener de todos los productos
api.get('/', async (req, res) => {
    let limit = req.query.limit // representa la cantidad de productos que se requieren
    let productsResult
    if (limit && !isNaN(limit)) {
        limitInt = parseInt(limit);
        productsResult = await productManagerMD.getProducts(limitInt)

    } else {
        productsResult = await productManagerMD.getProducts()
    }
    res.json(productsResult)
});

// Optener un producto especifico
api.get('/:pid', async (req, res) => {

    let pid = req.params.pid // ID del producto que se requiere

    if (pid) {
        const product = await productManagerMD.getProductById(pid)

        res.send(product)

    } else {
        res.send('Error: Se requiere el id del producto')
    }
});

// Añadir un producto
api.post('/', async (req, res) => {
    var data = req.body
    const result = await productManagerMD.addProduct(data)

    if (req.socketIO && req.socketIO.emit && (result.success == true)) {
        req.socketIO.emit('addProduct', data)
    }

    res.json(result)
})

// Actualizar los datos de un producto
api.put('/:pid', async (req, res) => {
    let pid = req.params.pid
    var data = req.body
    if (pid) {
        const result = await productManagerMD.updateProduct(pid, data)
        res.json(result)
    } else {
        res.send('Error: Se requiere el id del producto')
    }

})

// Borrar un producto
api.delete('/:pid', async (req, res) => {
    let pid = req.params.pid
    if (pid) {
        const result = await productManagerMD.deleteProduct(pid)

        if (req.socketIO && req.socketIO.emit && (result.success == true)) {
            req.socketIO.emit('removeProduct', pid)
        }

        res.json(result)

    } else {
        res.send('Error: Se requiere el id del producto')
    }

})


module.exports = api; // exporto la configuración

