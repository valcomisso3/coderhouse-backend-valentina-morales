const express = require('express')
const handlebars = require('express-handlebars');
const mongoose = require('mongoose')

const { Server } = require('socket.io')

// Clases
const ProductManager = require('./dao/fileSystem/productManager.js')
const CartsManager = require('./dao/fileSystem/cartsManager.js')
const ChatManagerDB = require('./dao/mongoDB/chatManagerDB.js')


// Direcciones
const address = require('./address.js')


// verifico que las carpetas que se van a utilizar para guardar informacion existan 
const productManager = new ProductManager(address.products_dirt) // Genero una instancia de ProductManager
productManager.folderControlForData() // Controlo que la ruta exista, si no existe la creo 
const cartsManager = new CartsManager(address.carts_dirt) // Genero una instancia de CartsManager
cartsManager.folderControlForData() // Controlo que la ruta exista, si no existe la creo 

const chatManagerDB = new ChatManagerDB()


// Importo las rutas
const products_routes = require('./routes/products.js');
const carts_routes = require('./routes/carts.js')
const views_routes = require('./routes/views.js');
const users_routes = require('./routes/users.router.js')

const app = express()

var port = address.port;

app.use(express.json())
app.use(express.urlencoded({ extended: true }))


// incorporo handlebars
app.engine('handlebars', handlebars.engine())
app.set('views', __dirname + '/views')
app.set('view engine', 'handlebars')
app.use(express.static(__dirname + '/public'))

// MiddleWares
const injectSocketServer = (req, res, next) => {
    req.socketIO = socketServer;
    next();
}

const injectData = async (req, res, next) => {
    // envio los productos que inicialmente se van a mostrar
    const products = await productManager.getProducts();
    const productsJson = JSON.stringify(products);
    req.productsJson = productsJson;

    // envio los mensajes que inicialmente se van a mostrar en el chat
    const messages = await chatManagerDB.getMessages()
    if (messages.success == true) {
        req.messagesJson = messages.value ? JSON.stringify(messages.value) : []
    } else {
        req.messagesJson = []
    }
    next();
}



// conecto base de datos
mongoose.connect(address.mongooseDB)
    .then(() => { console.log("Conectado a la base de datos") })
    .catch(error => console.error("Error en la conexion", error))



// agrego rutas
app.use('/api/products', injectSocketServer, products_routes); // rutas productos
app.use('/api/carts', carts_routes); // rutas carritos
app.use('/', injectData, views_routes); // rutas de mis pantallas
app.use('/api/users', users_routes) // rutas de usuarios

const httpServer = app.listen(port, () => { console.log(`servidor corriendo en http://localhost:${port}`); });

const socketServer = new Server(httpServer)



socketServer.on('connection', async socket => {

    socket.on("message", async data => {
        let resultAddMessage = await chatManagerDB.addMessage(data)
        if (resultAddMessage.success == true) {
            socketServer.emit("messageLogs", data)
        }
    })
})





