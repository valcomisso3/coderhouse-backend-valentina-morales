const socket = io()
let chatBox = document.getElementById("chatBox")
let user


Swal.fire({
    title: "Identificate",
    input: "text",
    text: "Ingresa tu nombre de usuario",

    inputValidator: (value) => {
        return !value && "Necesitas identificarte para ingresar a la sala"
    }

}).then(result => {
    user = result.value
    // usuario autenticado correctamente 
    socket.emit("newUser", user)
})


chatBox.addEventListener("keyup", evt => {
    if (evt.key === "Enter") {
        if (chatBox.value.trim().length > 0) {
            socket.emit("message", { user: user, message: chatBox.value })
            chatBox.value = ""
        }
    }
})

socket.on("newUserName", data => {
    console.log(`nuevo usuario ${data} `)

})

socket.on("messageLogs", data => {
    let log = document.getElementById('messageLogs')
    var div = document.createElement('div');

    div.innerHTML = `${data.user} dice: ${data.message}</br>`
    if (log.firstChild) {
        log.insertBefore(div, log.firstChild);
    } else {
        log.appendChild(div);
    }
})

document.addEventListener("DOMContentLoaded", function () {
    var messagesInit = window.messagesInit;
    showMessages(messagesInit)
})
async function showMessages(messages) {
    if (messages.length > 0) {
        messages.forEach(function (message) {


            var div = document.createElement('div');
            div.innerHTML = `${message.user} dice: ${message.message}</br>`

            let log = document.getElementById('messageLogs')
            if (log.firstChild) {
                log.insertBefore(div, log.firstChild);
            } else {
                log.appendChild(div);
            }

        });
    }

}

