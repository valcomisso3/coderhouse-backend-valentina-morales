
const socket = io()

socket.on('addProduct', data => {
    addProduct(data)
})

socket.on('removeProduct', data => {
    removeProduct(data)
})


document.addEventListener("DOMContentLoaded", function () {
    var products = window.products;
    mostrarProductos(products)
})

async function mostrarProductos(productos) {

    var listaProductos = document.getElementById('product-list');

    if (productos.length > 0) {
        productos.forEach(function (producto) {
            var li = document.createElement('li');
            li.classList.add('product');
            li.id = 'product_' + producto.id
            li.innerHTML = `
                <h2>${producto.title}</h2>
                <p>${producto.description}</p>
                <p>Precio: ${producto.price}</p>
                <p>Código: ${producto.code}</p>
                <p>Stock: ${producto.stock}</p>
                <p>Categoría: ${producto.category}</p>
                <p>Estado: ${producto.status}</p>
                <div class="thumbnails">
                    ${producto.thumbnail.map(image => `<img src="${image}" alt="${producto.title}">`).join('')}
                </div>
            `;
            listaProductos.appendChild(li);
        });
    } else {
        var li = document.createElement('li');
        li.id = 'product_0'
        li.classList.add('product');
        li.innerHTML = `
            <h2>No hay productos cargados</h2>
        `;
        return listaProductos.appendChild(li);
    }

}

async function addProduct(producto) {

    var listaProductos = document.getElementById('product-list');
    var cantidadElementos = listaProductos.getElementsByTagName("li").length;

    if (cantidadElementos <= 1) {
        var itemProduct = document.getElementById('product_0');
        if (itemProduct) itemProduct.remove()
    }
    var li = document.createElement('li');
    li.classList.add('product');
    li.id = 'product_' + producto.id
    li.innerHTML = `
        <h2>${producto.title}</h2>
        <p>${producto.description}</p>
        <p>Precio: ${producto.price}</p>
        <p>Código: ${producto.code}</p>
        <p>Stock: ${producto.stock}</p>
        <p>Categoría: ${producto.category}</p>
        <p>Estado: ${producto.status}</p>
        <div class="thumbnails">
            ${producto.thumbnail.map(image => `<img src="${image}" alt="${producto.title}">`).join('')}
        </div>
    `;

    listaProductos.appendChild(li)

}

async function removeProduct(productId) {
    var itemProduct = document.getElementById('product_' + productId);
    itemProduct.remove()

    var productList = document.getElementById("product-list");
    var cantidadElementos = productList.getElementsByTagName("li").length;

    if (cantidadElementos < 1) {

        var li = document.createElement('li');
        li.id = 'product_0'
        li.classList.add('product');
        li.innerHTML = `
            <h2>No hay productos cargados</h2>
        `;
        productList.appendChild(li);
        return
    }
}


