const path = require('path')

let products_dirt = path.join(__dirname, 'data/Products.json');
let carts_dirt = path.join(__dirname, 'data/Carts.json')
const port = 8080
const mongooseDB = "mongodb+srv://valentina:coder@coderhousecluster.flnx7nb.mongodb.net/ecommerce?retryWrites=true&w=majority&appName=CoderHouseCluster"


module.exports = {
    products_dirt,
    carts_dirt,
    port,
    mongooseDB
  };