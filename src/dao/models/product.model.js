const mongoose = require('mongoose')

const productCollection = "Products"

const productSchema = new mongoose.Schema({
    title: { type: String, required: true }, // titulo
    description: { type: String, required: true }, // descripcion
    price: { type: String, required: true }, // precio 
    code: { type: String, required: true, unique: true }, // codigo, este debe ser uinco para cada producto
    stock: { type: Number, required: true }, // cantidad en stock
    status: { type: String, required: true }, // estado
    category: { type: String, required: true }, // categoria
    thumbnail: { type: Array }, // array de imagenes, este no es un campo obligatorio

})

const productModel = mongoose.model(productCollection, productSchema)

module.exports = productModel