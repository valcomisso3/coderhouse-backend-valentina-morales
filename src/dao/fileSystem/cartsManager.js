
const fs = require('fs').promises

class CartsManager {
    constructor(pathDirection) {
        this.path = pathDirection
        this.folderPath = pathDirection.substring(0, pathDirection.lastIndexOf('/'));

        this.nextId = 0
    }


    // Metodos
    folderControlForData() {
        fs.readdir(this.folderPath).catch((error) => {
            if (error) {
                // No se encontro la carpeta, supongo que no existe y la inntento generar
                fs.mkdir(this.folderPath, { recursive: true }, (err) => {
                    if (err) {
                        // Ocurrio un error, no se pudo generar la carpeta.
                        // dialog.showErrorBox('error en generar la carpeta!!!!!!!!', err.toString())
                    } else {
                        // La carpeta se genero correctamente.
                    }
                });
            } else if (data) {
                // La carpeta ya existe
            }
        })
    }

    async addCart(cartValue) {
        // Parametros
        // cartValue = {
        //     products : [],
        // }
        const cartsList = await this.getCarts()


        var cart = cartValue; // campos recibidos del carrito



        const idCart = cartsList.length + 1 // codigo identificador unico por carrito

        this.nextId = idCart // actualizo mi orden de ids

        cart.id = idCart // le agrego el parametro id
        cart.products = cart.products ? cart.products : []

        try {
            let carts = cartsList
            carts.push(cart)
            await fs.writeFile(this.path, JSON.stringify(carts, null, 2))
            return `Se agrego el carrito correctamente id: ${idCart}`
        } catch (error) {
            console.error("Error al crear el carrito: ", error)
            return "Error al crear el carrito"
        }

    }

    async readCarts() {
        try {
            const data = await fs.readFile(this.path, 'utf8')
            return JSON.parse(data)
        } catch (error) {
            if (error.code === 'ENOENT') {
                return []
            } else {
                throw error
            }
        }
    }

    async getCarts() {
        try {
            const data = await this.readCarts()
            return data
        } catch (error) {
            return []
        }
    }

    async getCartsById(cart_id) {
        const data = await this.readCarts(); // Leo el archivo de productos
        const cartFound = data.find((cart) => cart.id == cart_id) // busco el producto po ID
        if (!cartFound) {
            console.error('Error: Not found')
            return { success: false, value: `Error: El carrito con el id ${cart_id} no existe` }
        }
        return { success: true, value: cartFound }
    }

    async addProductCart(cart_id, product_id, quantity) {
        let totalCarts = await this.readCarts() // datos de todos los productos

        let getCartValue = await this.getCartsById(cart_id); // datos de mi producto 

        if (getCartValue.success == false) {
            console.error(`No se encontro ningun carrito con ID ${cart_id}`)
            return `No se encontro ningun carrito con ID ${cart_id}`
        }

        // Aca se deberia revisar que el id de producto exista 

        let cartValue = getCartValue.value
        let productsCart = getCartValue.value.products

        let updateProduct = {}
        updateProduct.product = product_id

        const cartProductFound = productsCart.find((product) => product.product == product_id)

        if (!cartProductFound) {
            // no exite el producto , lo agrego
            updateProduct.quantity = quantity
            productsCart.push(updateProduct)
        } else {
            // el producto si existe
            productsCart = cartValue.products.map((product, index) => {
                if (product.product == product_id) {
                    updateProduct.quantity = product.quantity + quantity
                    return updateProduct
                } else {
                    return product
                }
            })
        }

        cartValue.products = productsCart

        let updateArrayCarts = totalCarts.map((cart) => {
            if (cart.id == cart_id) {
                return cartValue
            } else {
                return cart
            }
        })

        try {
            await fs.writeFile(this.path, JSON.stringify(updateArrayCarts, null, 2)) // sobrescribo el archivo con los datos actualizados
            return "Pruducto agregado al carrito correctamente"
        } catch (error) {
            return "Ocurrio un error al actualizar el carrito"
        }

    }




}

module.exports = CartsManager





