
const fs = require('fs').promises

class ProductManager {
    constructor(pathDirection) {
        this.path = pathDirection
        this.folderPath = pathDirection.substring(0, pathDirection.lastIndexOf('/'));
    }

    // Metodos

    folderControlForData() {
        fs.readdir(this.folderPath).catch((error) => {
            if (error) {
                // No se encontro la carpeta, supongo que no existe y la inntento generar
                fs.mkdir(this.folderPath, { recursive: true }, (err) => {
                    if (err) {
                        // Ocurrio un error, no se pudo generar la carpeta.
                        // dialog.showErrorBox('error en generar la carpeta!!!!!!!!', err.toString())
                    } else {
                        // La carpeta se genero correctamente.
                    }
                });
            } else if (data) {
                // La carpeta ya existe
            }
        })
    }

    async addProduct(productValue) {
        // Parametros
        // productValue = {
        //     title : 'nombre del producto',
        //     description : 'descripcion del producto',
        //     price: 'precio', 
        //     thumbnail: 'array de rutas de imagenes del producto', 
        //     code: 'code',
        //     stock: 'numero de piezas disponibles',
        //     category: 'categoria del producto',
        //     status: 'estado'
        // }
        const productsList = await this.getProducts()

        const requiredFields = ['title', 'description', 'price', 'code', 'stock', 'status', 'category'];  // campos obligatorios

        var product = productValue; // campos recibidos del producto

        // verifico si todos los campos obligatorios están presentes y no son nulos o vacíos
        const missingFields = requiredFields.filter(field => !(field in product && product[field]));

        if (missingFields.length > 0) {
            return { success: false, value: `Faltan los siguientes campos obligatorios: ${missingFields.join(', ')}` };
        }


        // verifico si el codigo del producto ya existe
        const existingCode = productsList.some(p => p.code === product.code);

        if (existingCode) {

            return { success: false, value: 'El código del producto ya existe.' };
        }

       

        const maxId = productsList.length > 0 ? productsList.reduce((max, obj) => {
            return obj.id > max ? obj.id : max;
        }, -Infinity) : 0

        const idProduct = maxId  + 1 // codigo identificador unico por producto

        product.id = idProduct // le agrego el parametro id


        try {
            let products = productsList
            products.push(product)
            await fs.writeFile(this.path, JSON.stringify(products, null, 2))
            return { success: true, value: 'Se agrego el producto correctamente' }
        } catch (error) {
            console.error("Error al crear el producto", error)
            return { success: false, value: "Error al crear el producto" }
        }

    }

    async leerProductos() {
        try {
            const data = await fs.readFile(this.path, 'utf8')
            return JSON.parse(data)
        } catch (error) {
            if (error.code === 'ENOENT') {
                return []
            } else {
                throw error
            }
        }
    }

    async getProducts() {
        try {
            const data = await this.leerProductos()
            return data
        } catch (error) {
            return []
        }
    }

    async getProductById(product_id) {

        const data = await this.leerProductos(); // Leo el archivo de productos

        const productFound = data.find((product) => product.id == product_id) // busco el producto po ID
        if (!productFound) {
            return { success: false, value: `Error: El producto con el id ${product_id} no existe` }
        }
        return { success: true, value: productFound }
    }

    async updateProduct(product_id, newData) {
        let totalProducts = await this.leerProductos() // datos de todos los productos

        let productgetValue = await this.getProductById(product_id); // datos de mi producto 

        if (productgetValue.success == false) {
            return `No se encontro ningun producto con ID ${product_id}`
        }

        let productValue = productgetValue.value
        let keysValues = Object.keys(newData) // array de campos a actualizar


        for (let index = 0; index < keysValues.length; index++) {
            let identifiquer = keysValues[index]
            if (identifiquer != 'id') productValue[identifiquer] = newData[identifiquer] // actualizo el producto siempre manteniendo su id
        }

        let updateArrayProduct = totalProducts.map((product) => {
            if (product.id == product_id) {
                return productValue
            } else {
                return product
            }
        })

        try {
            await fs.writeFile(this.path, JSON.stringify(updateArrayProduct, null, 2)) // sobrescribo el archivo con los datos actualizados
            return "Pruducto actualizado correctamente"
        } catch (error) {
            return "Ocurrio un error al actualizar el producto"
        }


    }

    async deleteProduct(id) {
        try {
            // Leer el archivo JSON
            let totalProducts = await this.leerProductos() // datos de todos los productos

            // Buscar el producto con el ID proporcionado
            const index = totalProducts.findIndex(product => product.id === id);
            if (index !== -1) {
                // Eliminar el producto completo
                totalProducts.splice(index, 1);

                // Escribir los datos actualizados en el archivo JSON
                await fs.writeFile(this.path, JSON.stringify(totalProducts, null, 2), 'utf8');
                return { success: true, value: 'Producto eliminado.' }
            } else {
                return { success: false, value: `No se encontró ningún producto con ID ${id}.` }
            }
        } catch (err) {
            console.error('Error:', err);
            return { success: false, value: 'Error al querer eliminar el producto' }
        }
    }

}

module.exports = ProductManager





