
// Modelos
const cartModel = require('../models/cart.model.js')

class CartsManagerDB {
  
    // Metodos
    async addCart(cartValue) {
        // Parametros
        // cartValue = {
        //     products : [],
        // }

        var cart = cartValue; // campos recibidos del carrito
        let productsarray = cart.products ? cart.products : []

        try {
            const resultDB = await cartModel.create({ productsarray })
            return resultDB
        } catch (error) {
            return "Error al crear el carrito"
        }

    }

    async getCarts() {
        try {
            let carts = await cartModel.find()
            return { success: true, value: carts }
        } catch (error) {
            return { success: false, value: error }
        }
    }

    async getCartsById(cart_id) {
        try {
            let cart = await cartModel.findOne({ _id: cart_id })

            if (cart.length == 0) {
                return { success: false, value: cart, message: `Error: El carrito con el id ${cart_id} no existe` }
            } else {
                return { success: true, value: cart }
            }
        } catch (error) {
            return { success: false, value: error, message: `Error: El carrito con el id ${cart_id} no existe` }
        }
    }

    async addProductCart(cart_id, product_id, quantity) {

        let getCartValue = await this.getCartsById(cart_id); // datos de mi producto 

        if (getCartValue.success == false) {
            return `No se encontro ningun carrito con ID ${cart_id}`
        }

        let cartValue = getCartValue.value
        let productsCart = getCartValue.value.products ? getCartValue.value.products : []

        let updateProduct = {}
        updateProduct.product = product_id

        const cartProductFound = productsCart.find((product) => product.product == product_id)

        if (!cartProductFound) {
            // no exite el producto , lo agrego
            updateProduct.quantity = quantity
            productsCart.push(updateProduct)
        } else {
            // el producto si existe
            productsCart = cartValue.products.map((product, index) => {
                if (product.product == product_id) {
                    updateProduct.quantity = product.quantity + quantity
                    return updateProduct
                } else {
                    return product
                }
            })
        }

        cartValue.products = productsCart

        try {

            let result = await cartModel.updateOne({ _id: cart_id }, cartValue)

            if (result.acknowledged == true && result.matchedCount == 1) {
                // cartModel:  {
                //     acknowledged: true,
                //     modifiedCount: 1,
                //     upsertedId: null,
                //     upsertedCount: 0,
                //     matchedCount: 1
                //   }
                return { success: true, message: "Pruducto agregado al carrito correctamente" }
            }

        } catch (error) {
            return "Ocurrio un error al actualizar el carrito"
        }

    }

}

module.exports = CartsManagerDB





