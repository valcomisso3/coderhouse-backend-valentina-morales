
const fs = require('fs').promises

// Modelos
const productModel = require('../models/product.model.js')


class ProductManager {
    constructor() {
    }

    // Metodos
    async addProduct(productValue) {
        // Parametros
        // productValue = {
        //     title : 'nombre del producto',
        //     description : 'descripcion del producto',
        //     price: 'precio', 
        //     thumbnail: 'array de rutas de imagenes del producto', 
        //     code: 'code',
        //     stock: 'numero de piezas disponibles',
        //     category: 'categoria del producto',
        //     status: 'estado'
        // }

        let objResult = {}
        try {

            objResult.success = true
            const resultDB = await productModel.create(productValue)
            objResult.message = 'El producto se añadio correctamente'
            objResult.value = resultDB

            return objResult
        } catch (error) {

            objResult.success = false
            if (error.code == 11000) {  // campos unicos repetidos, en este caso el code porque es el unico que debe ser UNICO
                objResult.message = "El codigo del producto ya existe"
                return objResult
            } else if (error.name == 'ValidationError') { // ocurrio un error al validar los campos requeridos 
                objResult.message = 'Faltan campos obligatorios'
                if (error.errors) {
                    let objError = Object.keys(error.errors)
                    objResult.requireFields = objError
                }
                return objResult
            } else {
                return error
            }
        }

    }

    async getProducts(limitNumber) {
        try {
            let products = await productModel.find().limit(limitNumber)
            return { success: true, value: products }
        } catch (error) {
            return { success: false, value: error }
        }
    }

    async getProductById(product_id) {
        try {
            let product = await productModel.find({ _id: product_id })

            if (product.length == 0) {
                return { success: false, value: product, message: `Error: El producto con el id ${product_id} no existe` }
            } else {
                return { success: true, value: product }
            }
        } catch (error) {
            return { success: false, value: error, message: `Error: El producto con el id ${product_id} no existe` }
        }
    }

    async updateProduct(product_id, newData) {

        try {

            let result = await productModel.updateOne({ _id: product_id }, newData)
            if (result.acknowledged == true && result.matchedCount == 0) {
                // Pruducto No encontrado
                return { success: false, message: `No se encontro ningun producto con ID ${product_id}` }
            } else if (result.acknowledged == true) {
                // Pruducto actualizado correctamente
                return { success: true, message: 'Pruducto actualizado correctamente' }
            } else {
                // no se puede actualizar el producto
                return { success: false, message: 'No es posible actualizar el producto', onlySupportedFields: ['title', 'description', 'price', 'code', 'stock', 'status', 'category', 'thumbnail'] }
            }
        } catch (error) {
            return { success: false, message: "Ocurrio un error al actualizar el producto" }
        }


    }

    async deleteProduct(product_id) {
        try {
            let result = await productModel.deleteOne({ _id: product_id })
            if (result.acknowledged == true && result.deletedCount == 1) {
                // se elimino el producto correctamente
                return { success: true, value: 'Producto eliminado.' }
            } else if (result.acknowledged == true && result.deletedCount == 0) {
                // no se encontro el producto
                return { success: false, value: `No se encontró ningún producto con ID ${product_id} para ser eliminado.` }
            }
        } catch (err) {
            console.error('Error:', err);
            return { success: false, message: 'Error al querer eliminar el producto', value: err }
        }
    }

}

module.exports = ProductManager





