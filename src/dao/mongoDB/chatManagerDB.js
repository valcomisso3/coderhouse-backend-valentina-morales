

// Modelos
const messageModel = require('../models/message.model.js')

class ChatManagerDB {
    // Metodos
    async addMessage(user, message) {
        // Parametros
        let objResult = {}
        try {
            objResult.success = true
            const resultDB = await messageModel.create(user, message)
            objResult.message = 'Mensaje añadido correctamente'
            objResult.value = resultDB
            return objResult
        } catch (error) {
            objResult.success = false
            objResult.value = error
            return objResult
        }

    }

    async getMessages() {
        try {
            let messages = await messageModel.find()
            return { success: true, value: messages }
        } catch (error) {
            return { success: false, value: error }
        }
    }
}

module.exports = ChatManagerDB





